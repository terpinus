#!/usr/bin/perl

%names = (
    'Aacute' => 'A\'',
    'aacute' => 'a\'',
    'Agrave' => 'A`',
    'agrave' => 'a`',
    'uni1EA2' => 'A?',
    'uni1EA3' => 'a?',
    'Atilde' => 'A~',
    'atilde' => 'a~',
    'uni1EA0' => 'A.',
    'uni1EA1' => 'a.',

    'Acircumflex' => 'A^',
    'acircumflex' => 'a^',

    'uni1EA4' => 'A^\'',
    'uni1EA5' => 'a^\'',
    'uni1EA6' => 'A^`',
    'uni1EA7' => 'a^`',
    'uni1EA8' => 'A^?',
    'uni1EA9' => 'a^?',
    'uni1EAA' => 'A^~',
    'uni1EAB' => 'a^~',
    'uni1EAC' => 'A^.',
    'uni1EAD' => 'a^.',

    'Abreve' => 'A(',
    'abreve' => 'a(',

    'uni1EAE' => 'A(\'',
    'uni1EAF' => 'a(\'',
    'uni1EB0' => 'A(`',
    'uni1EB1' => 'a(`',
    'uni1EB2' => 'A(?',
    'uni1EB3' => 'a(?',
    'uni1EB4' => 'A(~',
    'uni1EB5' => 'a(~',
    'uni1EB6' => 'A(.',
    'uni1EB7' => 'a(.',

    'Eacute' => 'E\'',
    'eacute' => 'e\'',
    'Egrave' => 'E`',
    'egrave' => 'e`',
    'uni1EBA' => 'E?',
    'uni1EBB' => 'e?',
    'Etilde' => 'E~',
    'etilde' => 'e~',
    'Edotbelow' => 'E.',
    'edotbelow' => 'e.',

    'Ecircumflex' => 'E^',
    'ecircumflex' => 'e^',

    'uni1EBE' => 'E^\'',
    'uni1EBF' => 'e^\'',
    'uni1EC0' => 'E^`',
    'uni1EC1' => 'e^`',
    'uni1EC2' => 'E^?',
    'uni1EC3' => 'e^?',
    'uni1EC4' => 'E^~',
    'uni1EC5' => 'e^~',
    'uni1EC6' => 'E^.',
    'uni1EC7' => 'e^.',

    'Iacute' => 'I\'',
    'iacute' => 'i\'',
    'Igrave' => 'I`',
    'igrave' => 'i`',
    'uni1EC8' => 'I?',
    'uni1EC9' => 'i?',
    'Itilde' => 'I~',
    'itilde' => 'i~',
    'uni1ECA' => 'I.',
    'uni1ECB' => 'i.',

    'Oacute' => 'O\'',
    'oacute' => 'o\'',
    'Ograve' => 'O`',
    'ograve' => 'o`',
    'uni1ECE' => 'O?',
    'uni1ECF' => 'o?',
    'Otilde' => 'O~',
    'otilde' => 'o~',
    'Odotbelow' => 'O.',
    'odotbelow' => 'o.',

    'Ocircumflex' => 'O^',
    'ocircumflex' => 'o^',

    'uni1ED0' => 'O^\'',
    'uni1ED1' => 'o^\'',
    'uni1ED2' => 'O^`',
    'uni1ED3' => 'o^`',
    'uni1ED4' => 'O^?',
    'uni1ED5' => 'o^?',
    'uni1ED6' => 'O^~',
    'uni1ED7' => 'o^~',
    'uni1ED8' => 'O^.',
    'uni1ED9' => 'o^.',

    'Ohorn' => 'O+',
    'ohorn' => 'o+',

    'uni1EDA' => 'O+\'',
    'uni1EDB' => 'o+\'',
    'uni1EDC' => 'O+`',
    'uni1EDD' => 'o+`',
    'uni1EDE' => 'O+?',
    'uni1EDF' => 'o+?',
    'uni1EE0' => 'O+~',
    'uni1EE1' => 'o+~',
    'uni1EE2' => 'O+.',
    'uni1EE3' => 'o+.',

    'Uacute' => 'U\'',
    'uacute' => 'u\'',
    'Ugrave' => 'U`',
    'ugrave' => 'u`',
    'uni1EE6' => 'U?',
    'uni1EE7' => 'u?',
    'Utilde' => 'U~',
    'utilde' => 'u~',
    'uni1EE4' => 'U.',
    'uni1EE5' => 'u.',

    'Uhorn' => 'U+',
    'uhorn' => 'u+',

    'uni1EE8' => 'U+\'',
    'uni1EE9' => 'u+\'',
    'uni1EEA' => 'U+`',
    'uni1EEB' => 'u+`',
    'uni1EEC' => 'U+?',
    'uni1EED' => 'u+?',
    'uni1EEE' => 'U+~',
    'uni1EEF' => 'u+~',
    'uni1EF0' => 'U+.',
    'uni1EF1' => 'u+.',

    'Yacute' => 'Y\'',
    'yacute' => 'y\'',
    'Ygrave' => 'Y`',
    'ygrave' => 'y`',
    'uni1EF6' => 'Y?',
    'uni1EF7' => 'y?',
    'Ytilde' => 'Y~',
    'ytilde' => 'y~',
    'uni1EF4' => 'Y.',
    'uni1EF5' => 'y.',

    'Dcroat' => 'DD',
    'dcroat' => 'dd',
);

while (<>) {
	if (m/^([0-9A-F]+)$/) {
		$full = $&;
		$t = $1;
		$t =~ s/0/......../g;
		$t =~ s/1/......##/g;
		$t =~ s/2/....##../g;
		$t =~ s/3/....####/g;
		$t =~ s/4/..##..../g;
		$t =~ s/5/..##..##/g;
		$t =~ s/6/..####../g;
		$t =~ s/7/..######/g;
		$t =~ s/8/##....../g;
		$t =~ s/9/##....##/g;
		$t =~ s/A/##..##../g;
		$t =~ s/B/##..####/g;
		$t =~ s/C/####..../g;
		$t =~ s/D/####..##/g;
		$t =~ s/E/######../g;
		$t =~ s/F/########/g;
		print "$full\t$t\n";
	}
	elsif (m/^STARTCHAR (.*)/ && defined($names{$1})) {
		print "STARTCHAR $names{$1}\n";
	}
	else {
		print $_;
	}
}
