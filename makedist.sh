#!/bin/sh

T=$1
if [ -z "$T" ]; then
	echo "Need a tag"
	exit
fi
V=$(echo $T|sed 's,^v,,')
git archive --format=tar --prefix=terpinus-font-$V/ $T|gzip > terpinus-font-$V.tar.gz
md5sum terpinus-font-$V.tar.gz > terpinus-font-$V.tar.gz.md5sum
sha1sum terpinus-font-$V.tar.gz > terpinus-font-$V.tar.gz.sha1sum
